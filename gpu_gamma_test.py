"""
Copyright 2013 Scott Linderman (scott.linderman@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Description:
Test the sample_gamma.cu kernel.

Updated: 
2/19/2013
"""

import pycuda.autoinit
import pycuda.compiler as nvcc
import pycuda.driver as cuda
import pycuda.gpuarray as gpuarray
import pycuda.curandom as curandom

import numpy as np
import scipy.io 
import time
import os
import io

# Global params
cu_src = os.path.join("sample_gamma.cu")
M = 32
B = 1024

# Constants
ERROR_SUCCESS            = 0
ERROR_INVALID_PARAMETER  = 1
ERROR_SAMPLE_FAILURE     = 2

def compileKernels(srcFile, kernelNames, srcParams=None):
    """
    Load the GPU kernels from the specified CUDA C file 
    """
    
    # Read the src file into a string
    custr = ""
    with io.open(srcFile, 'r') as file:
        for l in file:
            custr += l
    
    ## Replace consts in cu file
    if srcParams != None:
        custr = custr % srcParams
    
    # Compile the CUDA Kernel
    cu_kernel_source_module = nvcc.SourceModule(custr)
    
    # Load the kernels into a dictionary
    kernels = {}
    for name in kernelNames:
        kernels[name] = cu_kernel_source_module.get_function(name)
        
    return kernels

def initializeCuRand():
    """
    Initialize a CuRand PRNG. Use the XORWOW PRNG, which is slightly less 
    accurate than the Mersenne Twister algorithm, but is the best PRNG 
    exposed by the pycuda library.
    """
    rand_gpu = curandom.XORWOWRandomNumberGenerator()
    return rand_gpu
    
def initializeGpuData(N):
    """
    Initialize memory on the GPU
    
    N - the number of gamma RVs to sample per iteration
    M - the number of threads per gamma RV
    """
    N = int(N)
    gpudata = {}
    # Allocate space for the normal and uniform RVs required by 
    # the Marsaglia-Tsang algorithm
    gpudata["N"] = gpuarray.empty((N*M,), dtype=np.float32)
    gpudata["U"] = gpuarray.empty((N*M,), dtype=np.float32)
    gpudata["Gtemp"] = gpuarray.empty((N*M,), dtype=np.float32)
    gpudata["accept"] = gpuarray.empty((N*M,), dtype=np.int32)
    
    # Allocate space for params
    gpudata["a"] = gpuarray.empty((N,), dtype=np.float32)
    gpudata["b"] = gpuarray.empty((N,), dtype=np.float32)
    
    # Allocate space for the output
    gpudata["G"] = gpuarray.empty((N,), dtype=np.float32)
    gpudata["status"] = gpuarray.empty((N,), dtype=np.int32)
    gpudata["status"].fill(-1)   
    
    return gpudata

def run_test(kernels, rand_gpu, N, prng=True, acc=True, red=True, copy=True, check=True):
    """
    Test a GPU implementation of Marsaglia's algorithm for different numbers of 
    variates per call. On Unix systems time.clock() is much more accurate 
    (1/100th sec resolution) than time.time(). 
    Conversely, on Windows, time.time() has higher resolution.
    """
    iters = 1000
#    N = 2**np.arange(10,21)
#    iters = 1
#    N = [2**20]
    
    # Track the runtime
    T = np.zeros(np.size(N))
    
    # Track the failure rate
    n_total_failures = 0
    n_samples  = iters*np.sum(N)
        
    # Initialize gamma parameters
    a = 2
    b = 0.2
    
    for (j,n) in enumerate(N):
        
        # Allocate memory for each sample
        gpudata = initializeGpuData(n)
        
        # Choose random gamma params
        a = np.random.randint(1,100, size=(n,))
        b = np.ones((n,))
        gpudata["a"].set(a.astype(np.float32))
        gpudata["b"].set(b.astype(np.float32))
        
        # Initialize random uniform and normal variates (esp when prng=False)
        rand_gpu.fill_uniform(gpudata["U"])
        rand_gpu.fill_normal(gpudata["N"])
        
        # Determine number of threads/blocks to launch
        grid_w = int(np.ceil(float(n)*M/B))
        
        # Create timing events
        t_start = cuda.Event()
        t_stop = cuda.Event()
        
        t_start.record()
        t_start.synchronize()
        for i in np.arange(iters):
            if prng:
                # Sample Normal and Uniform RVs for each thread
                rand_gpu.fill_uniform(gpudata["U"])
                rand_gpu.fill_normal(gpudata["N"])
            
            # Call the kernel
#            kernels["gamrnd"](np.int32(n),
#                              gpudata["U"].gpudata,
#                              gpudata["N"].gpudata,
#                              gpudata["a"].gpudata,
#                              gpudata["b"].gpudata,
#                              gpudata["G"].gpudata,
#                              gpudata["status"].gpudata,
#                              block=(B,1,1),
#                              grid=(grid_w,1))

            if acc:
                # Call the kernel to decide whether each thread's sample is accepted
                kernels["gamrnd_acc"](np.int32(n),
                                  gpudata["U"].gpudata,
                                  gpudata["N"].gpudata,
                                  gpudata["a"].gpudata,
                                  gpudata["b"].gpudata,
                                  gpudata["Gtemp"].gpudata,
                                  gpudata["accept"].gpudata,
                                  block=(B,1,1),
                                  grid=(grid_w,1))
            
            
            if red:
                # Reduce the per-thread gamma samples to get a final sample
                kernels["gamrnd_red"](np.int32(n),
                                  gpudata["Gtemp"].gpudata,
                                  gpudata["accept"].gpudata,
                                  gpudata["G"].gpudata,
                                  gpudata["status"].gpudata,
                                  block=(B,1,1),
                                  grid=(grid_w,1))

            
            if copy:
                # Get the result
                G = gpudata["G"].get()
            
            if check:
                status = gpudata["status"].get()
                                
                n_success = np.count_nonzero(status==ERROR_SUCCESS)                
                n_failures = np.count_nonzero(status==ERROR_SAMPLE_FAILURE)
                n_total_failures += n_failures
                
                gpudata["status"].fill(-1)
                    
        t_stop.record()
        t_stop.synchronize()
        
        t_elapsed = t_start.time_till(t_stop)*1e-3
        
        T[j] = (t_elapsed)/iters
        print "Average time/%d samples: %.4f" % (n,T[j])
                
        
    # Average the failure rate
    f = float(n_total_failures)/n_samples
    print "Average failures/sample: %.10f" % f
    
    
    return (T,f)   
    
def run_correctness_test(kernels, rand_gpu):
    """
    Run a correctness test to ensure the gpu gamrnd implementation
    matches the true distribution
    """
    print "Running correctness test"
    # Set params
    a = 2.5
    b = 0.5
    n = 2**20
    
    # Allocate memory for each sample
    gpudata = initializeGpuData(n)
    gpudata["a"].fill(a)
    gpudata["b"].fill(b)

    # Determine number of threads/blocks to launch
    grid_w = int(np.ceil(float(n)*M/B))

    # Sample RVs
    # Sample Normal and Uniform RVs for each thread
    rand_gpu.fill_uniform(gpudata["U"])
    rand_gpu.fill_normal(gpudata["N"])
    
    # Call the kernel
#    kernels["gamrnd"](np.int32(n),
#                      gpudata["U"].gpudata,
#                      gpudata["N"].gpudata,
#                      gpudata["a"].gpudata,
#                      gpudata["b"].gpudata,
#                      gpudata["G"].gpudata,
#                      gpudata["status"].gpudata,
#                      block=(B,1,1),
#                      grid=(grid_w,1))

    # Call the kernel
    kernels["gamrnd_acc"](np.int32(n),
                      gpudata["U"].gpudata,
                      gpudata["N"].gpudata,
                      gpudata["a"].gpudata,
                      gpudata["b"].gpudata,
                      gpudata["Gtemp"].gpudata,
                      gpudata["accept"].gpudata,
                      block=(B,1,1),
                      grid=(grid_w,1))
    
    
    # Reduce the per-thread gamma samples to get a final sample
    kernels["gamrnd_red"](np.int32(n),
                      gpudata["Gtemp"].gpudata,
                      gpudata["accept"].gpudata,
                      gpudata["G"].gpudata,
                      gpudata["status"].gpudata,
                      block=(B,1,1),
                      grid=(grid_w,1))
    
    # Get the result
    G = gpudata["G"].get()
    status = gpudata["status"].get()
    assert np.all(status==ERROR_SUCCESS)
    
    plot_histogram(G,a,b)
    

def plot_histogram(G, a, b):
    """
    Plot a normalized histogram of the samples compared to the true pdf
    """
    import matplotlib
    # Force matplotlib to not use any Xwindows backend.
    matplotlib.use('Agg')
                   
    import matplotlib.pyplot as plt
#    # Force matplotlib to not use any Xwindows backend.
#    matplotlib.use('Agg')

    import scipy.special as sps
    
    # Bin the samples
    count, bins, ignored = plt.hist(G, 50, normed=True)
    
    # Calculate the pdf of each bin
    y = (bins**(a-1))*(b**a)/(sps.gamma(a))*np.exp(-bins*b)
    
    # Plot the pdf over the empiral histogram
    plt.plot(bins, y, linewidth=2, color='r')
    plt.xlabel("x")
    plt.ylabel("Pr(x)")
    plt.title("2^{20} GPU gamrnd samples ~ G(%.1f,%.1f)" % (a,b))
    plt.savefig("gamma_%.2f_%.2f_hist.png" % (a,b))
    
#    scipy.io.savemat("gpu_smpls.mat", {"G":G, "a":a, "b":b}, oned_as="column")

def save_results(N,T,f, name=None):
    """
    Save results to a .mat file
    """
    if name==None:
        name = "gpu_gamma_res_M%d_red.mat" % M
    scipy.io.savemat(name, {"N":N, "T":T, "f":f}, oned_as="column")
    

if __name__ == "__main__":
    kernels = compileKernels(cu_src, ["gamrnd", "gamrnd_acc", "gamrnd_red"], srcParams={"M":M, "B":B})
    
    rand_gpu = initializeCuRand()
    
#    run_correctness_test(kernels, rand_gpu)

    # Test gpu breakdown for fixed N 
    N = np.array([2**16])
    
    (T,f) = run_test(kernels, rand_gpu, N, prng=True, acc=False, red=False, copy=False, check=False)
    save_results(N,T,f,"gpu_gamma_res_M%d_N%d_prng.mat" % (M,N[0]))
    
    (T,f) = run_test(kernels, rand_gpu, N, prng=False, acc=True, red=False, copy=False, check=False)
    save_results(N,T,f,"gpu_gamma_res_M%d_N%d_acc.mat" % (M,N[0]))
    
    (T,f) = run_test(kernels, rand_gpu, N, prng=False, acc=False, red=True, copy=False, check=False)
    save_results(N,T,f,"gpu_gamma_res_M%d_N%d_red.mat" % (M,N[0]))
    
    (T,f) = run_test(kernels, rand_gpu, N, prng=False, acc=False, red=False, copy=True, check=False)
    save_results(N,T,f,"gpu_gamma_res_M%d_N%d_copy.mat" % (M,N[0]))

    
    